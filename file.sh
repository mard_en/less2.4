DEPLOY_ID=$(curl -k --location --header "PRIVATE-TOKEN:$api_token" "https://gitlab.com/api/v4/projects/25720738/pipelines" | jq '.[] | .id' | head -1)
JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN:$api_token" "https://gitlab.com/api/v4/projects/25720738/pipelines/$DEPLOY_ID/jobs" | jq '.[] | select(.name == "build_prod") | .id')
curl --output main.log --location --header "PRIVATE-TOKEN:$api_token" "http://gitlab.com/api/v4/projects/25720738/jobs/$JOB_ID/artifacts/main.log"
